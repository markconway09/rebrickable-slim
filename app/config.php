<?php
require_once '../vendor/autoload.php';

define('APP_DIR', __DIR__);
define('BASE_DIR', realpath(__DIR__ . '/..'));
define('TEMPLATES_DIR', APP_DIR . '/templates');
define('DATA_DIR', APP_DIR . '/data');

define('REBRICKABLE_URL_BASE', "https://rebrickable.com/api/v3/lego/");
define('REBRICKABLE_API_KEY_FILE', BASE_DIR . '/apikey.txt');