<?php
use Slim\Factory\AppFactory;

require_once 'config.php';

$app = AppFactory::create();

$app->get('/', '\Lego\Controllers\LegoController:mGETaSearch');
$app->get('/search', '\Lego\Controllers\LegoController:mGETaSearch');
$app->get('/set/{set_num}', '\Lego\Controllers\LegoController:mGETaSet');
$app->get('/part/{part_num}/{color_id}', '\Lego\Controllers\LegoController:mGETaPart');
$app->get('/hello/{name}', '\Lego\Controllers\HelloController:hello');

$app->run();