<?php
namespace Lego\External;

class ApiRebrickable{
    private function _call($url) {
        $ch = curl_init($url);
        $apiKey = file_get_contents(REBRICKABLE_API_KEY_FILE);
        if ($apiKey === false) {
            die("ERROR: No API KEY file!");
        }
        $headers = [ 'Authorization: key ' . $apiKey ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        if ($result === false) {
            die("ERROR: " . curl_error($ch));
        }
        $result = json_decode($result);
        return $result;
    }

    public function single($path) {
        $url = REBRICKABLE_URL_BASE . $path;
        return $this->_call($url);
    }

    public function multiple($path) {
        $url = REBRICKABLE_URL_BASE . $path;
        $results = [];
        while($url) {
            $page = $this->_call($url);
            if (!isset($page->results)) {
                die("ERROR: API CALL is not multi-page");
            }
            $results = array_merge($results, $page->results);
            $url = $page->next;
        }
        return $results;
    }
};