<?php
namespace Lego\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class HelloController {
    public function hello(Request $request, Response $response, array $args){
        $name=$args['name'];
        $response->getBody()->write("Hello, $name");
        return $response;
    }
}