<?php
namespace Lego\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Lego\External\ApiRebrickable;

class LegoController {

  private $twig;
  private $api;

  public function __construct(){
    // Twig initialization
    $loader = new \Twig\Loader\FilesystemLoader(TEMPLATES_DIR);
    $this->twig = new \Twig\Environment($loader);
    $this->api = new \ApiRebrickable();
  }

  public function mGETaSearch(Request $request, Response $response, array $args) {
    $data = [];
    $params=$request->getQueryParams();
    if (isset($params['search'])) {
      $search = $params['search'];
      $sets = $this->api->multiple("sets/?search=" . $search);
      if (!$sets) die("Error de rebrickable");
      foreach($sets as $set) {
        $sets[] = [
          'set_num' => $set->set_num,
          'name' => $set->name,
          'set_img_url' => $set->set_img_url,
        ];
      }
      $data = [
        'search' => $search,
        'sets' => $sets,
      ];

    }
    $html = $this->twig->render('lego_search.twig', $data);
    $response->getBody()->write($html);
    return $response;
  }

  public function mGETaSet(Request $request, Response $response, array $args) {
    $data = [];
    if (isset($args['set_num'])) {
      $set_num = $args['set_num'];
      $set = $this->api->single("sets/" . $set_num . "/");
      if (!$set || !isset($set->set_num)) die("Error de rebrickable: " . json_encode($set));

      $parts = $this->api->multiple("sets/" . $set->set_num . "/parts/");
      if (!$parts) die("Error de rebrickable");
      foreach($parts as &$part) {
        $part = [
          'part_num' => $part->part->part_num,
          'part_name' => $part->part->name,
          'part_img_url' => $part->part->part_img_url,
          'color_id' => $part->color->id,
          'quantity' => $part->quantity,
          'num_sets' => $part->num_sets,
        ];
      }

      $data = [
        'set' => $set,
        'parts' => $parts,
      ];
    }
    $html = $this->twig->render('lego_set.twig', $data);
    $response->getBody()->write($html);
    return $response;
  }

  public function mGETaPart(Request $request, Response $response, array $args) {
    $data = [];
    if (isset($args['part_num']) && isset($args['color_id'])) {
      $part_num = $args['part_num'];
      $color_id = $args['color_id'];

      $part = $this->api->single("parts/" . $part_num . "/");
      if (!$part || !isset($part->part_num)) die("Error de rebrickable: " . json_encode($part));

      $part2 = $this->api->single("parts/" . $part_num . "/colors/" . $color_id . "/");
      if (!$part2 || !isset($part2->part_img_url)) die("Error de rebrickable: " . json_encode($part2));

      $sets = $this->api->multiple("parts/" . $part_num . "/colors/" . $color_id . "/sets/");
      if (!$sets) die("Error de rebrickable");
      foreach($sets as &$set) {
        $set = [
          'set_num' => $set->set_num,
          'name' => $set->name,
          'set_img_url' => $set->set_img_url,
        ];
      }

      $data = [
        'part' => $part,
        'part2' => $part2,
        'sets' => $sets,
      ];
    }
    $html = $this->twig->render('lego_part.twig', $data);
    $response->getBody()->write($html);
    return $response;
  }
}